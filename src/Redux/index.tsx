import { createStore, applyMiddleware, combineReducers } from "redux";
import thunk from "redux-thunk";
import logger from "redux-logger";

import { UserDataReducer } from "./reducers/user";
import { CategoryDataReducer } from "./reducers/category";
import { CartDataReducer } from "./reducers/cartData";
import { CheckoutReducer } from "./reducers/checkOut";

import throttle from "lodash.throttle";

const initialState = JSON.parse(localStorage.getItem("Neostore") || "{}");

//create store
const store = createStore(
  combineReducers({
    UserDataReducer,
    CategoryDataReducer,
    CartDataReducer,
    CheckoutReducer
  }),
  initialState,
  applyMiddleware(thunk, logger)
);

store.subscribe(
  throttle(() => {
    localStorage.setItem("Neostore", JSON.stringify(store.getState()));
  }, 1000)
);
export default store;
