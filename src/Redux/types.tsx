export const USER_DATA = "USER_DATA";
export const REMOVE_CART = "REMOVE_CART";
export const CATEGORY_DATA = "CATEGORY_DATA";
export const CART_DATA = "CART_DATA";
export const QUANTITY_INC = "QUANTITY_INC";
export const QUANTITY_DEC = "QUANTITY_DEC";
export const LOADING = "LOADING";
export const CHECKOUT = "CHECKOUT";
export const EMPTY_CART = "EMPTY_CART";

interface UserDataAction {
  type: typeof USER_DATA;
  payload: any;
}
interface CartRemoveAction {
  type: typeof REMOVE_CART | typeof EMPTY_CART;
  id?: string;
}
interface CategoryDataAction {
  type: typeof CATEGORY_DATA;
  payload: any;
}
interface CartDataAction {
  type: typeof CART_DATA;
  payload: any;
}
interface QuantityAction {
  type: typeof QUANTITY_INC | typeof QUANTITY_DEC;
  productId: string;
}
interface CheckoutAction {
  type: typeof CHECKOUT;
  orderId: string;
}
interface LoadingAction {
  type: typeof LOADING;
}

export type UserActionTypes =
  | UserDataAction
  | CartRemoveAction
  | CategoryDataAction
  | CartDataAction
  | QuantityAction
  | CheckoutAction
  | LoadingAction;

// Checkout actions

export const T = {
  CHECKOUT_INIT: "CHECKOUT_INIT",
  CHECKOUT_SUCCESS: "CHECKOUT_SUCCESS",
  CHECKOUT_ERROR: "CHECKOUT_ERROR"
};

export interface CheckOutActions {
  type:
    | typeof T.CHECKOUT_INIT
    | typeof T.CHECKOUT_SUCCESS
    | typeof T.CHECKOUT_ERROR;
  payload?: any;
}
