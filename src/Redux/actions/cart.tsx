import * as action_types from "../types";

export const cartData = (cartData: any) => {
  return {
    type: action_types.CART_DATA,
    data: cartData
  };
};
export const RemoveData = (_id: any) => {
  return {
    type: action_types.REMOVE_CART,
    id: _id
  };
};
export const quantityInc = (productId: any) => {
  return {
    type: action_types.QUANTITY_INC,
    productId
  };
};
export const quantityDec = (productId: any) => {
  return {
    type: action_types.QUANTITY_DEC,
    productId
  };
};
export const cartDel = (data: any) => {
  return {
    type: action_types.CHECKOUT,
    data
  };
};
