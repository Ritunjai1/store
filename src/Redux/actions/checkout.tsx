import { T } from "../types";
import axios from "axios";

// Redux thunk
const initCheckout = (data: any) => {
  return (dispatch: any) => {
    dispatch(init()); // Initiate checkout process (Loaders will start spinning)
    // API Call
    axios
      .post("http://10.0.100.228:3000/cart", data, {
        headers: {
          Authorization: localStorage.getItem("token")
        }
      })
      .then(response => {
        // Success Actions
        dispatch(success(response.data));
        dispatch({ type: "EMPTY_CART" });
      })
      .catch(err => {
        // Error Actions
        dispatch(error(err.response ? err.response.data : err));
      });
  };

  function init() {
    return {
      type: T.CHECKOUT_INIT
    };
  }

  function success(res: any) {
    return {
      type: T.CHECKOUT_SUCCESS,
      payload: res
    };
  }

  function error(error: any) {
    return {
      type: T.CHECKOUT_ERROR,
      payload: error
    };
  }
};

// Export default package
export default initCheckout;
