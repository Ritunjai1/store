import { USER_DATA, UserActionTypes } from "../types";

const initialState = { isLoggedIn: false };

export const UserDataReducer = (
  state = initialState,
  action: UserActionTypes
) => {
  switch (action.type) {
    case USER_DATA:
      console.log(action.payload);
      return {
        ...action.payload,
        isLoggedIn: true
      };

    default:
      return state;
  }
};
