import { CheckOutActions, T } from "../types";
import { ToastsStore } from "react-toasts";
import { navigate } from "@reach/router";

const initialState: object = {
  error: null,
  isLoading: false
};

export const CheckoutReducer = (
  state = initialState,
  action: CheckOutActions
) => {
  switch (action.type) {
    case T.CHECKOUT_INIT: {
      return { ...state, isLoading: true };
    }

    case T.CHECKOUT_SUCCESS: {
      ToastsStore.success(action.payload.message);
      navigate("/order/Placed");
      return { ...state, ...action.payload, isLoading: false };
    }

    case T.CHECKOUT_ERROR: {
      ToastsStore.error(action.payload.message);
      return {
        ...state,
        isLoading: false,
        error: action.payload
      };
    }

    default:
      return state;
  }
};
