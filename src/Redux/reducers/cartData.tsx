import {
  CART_DATA,
  UserActionTypes,
  REMOVE_CART,
  QUANTITY_INC,
  QUANTITY_DEC,
  CHECKOUT,
  EMPTY_CART,
  LOADING
} from "../types";
import Lazy from "lazy.js";
import { ToastsContainer, ToastsStore } from "react-toasts";

const initialState: object[] = [];

export const CartDataReducer = (
  state = initialState,
  action: UserActionTypes
) => {
  switch (action.type) {
    // Add Product to cart
    case CART_DATA:
      const res = Lazy(state).findWhere({ _id: action.payload._id });
      if (res) {
        ToastsStore.warning("Already in cart");
        return state;
      }
      ToastsStore.success("Added to cart");
      console.log(action.payload);
      return [...state, { ...action.payload, qty: 1 }];

    // Remove product from cart
    case REMOVE_CART:
      console.log(action);
      ToastsStore.success("Deleted");
      return state.filter((item: any) => item._id !== action.id);

    case EMPTY_CART:
      console.log("Cart");
      return [];

    // Quantity Increase
    case QUANTITY_INC:
      const result: any = state.map((product: any) => {
        if (product._id === action.productId) {
          return { ...product, qty: product.qty + 1 };
        }
        return product;
      });

      console.log(result);
      return result;

    //Quantity Decrease
    case QUANTITY_DEC:
      const dec: any = state.map((product: any) => {
        if (product._id === action.productId) {
          return { ...product, qty: product.qty - 1 > 0 ? product.qty - 1 : 1 };
        }
        return product;
      });
      //default
      return dec;

    default:
      return state;
  }
};
