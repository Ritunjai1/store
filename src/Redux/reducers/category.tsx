import { CATEGORY_DATA, UserActionTypes } from "../types";

const initialState: object[] = [];

export const CategoryDataReducer = (
  state = initialState,
  action: UserActionTypes
) => {
  switch (action.type) {
    case CATEGORY_DATA:
      return [...action.payload];
    default:
      return state;
  }
};
