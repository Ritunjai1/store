import React, { Component } from "react";
import "./App.css";
import Header from "./Components/Header/header";
import Corausel from "./Screens/Corausel/Corausel";
import Footer from "./Components/Foot/Footer";
import { Router } from "@reach/router";
import List from "./Screens/Product_list/pro_list";
import Cart from "./Screens/Cart/cart";
import Details from "./Screens/Product_list/pro_details";
import Profile from "./Screens/Profile/Profile";
import Orders from "./Screens/Orders/Orders";
import Address from "./Screens/Address/Address";
import Login from "./Screens/Login/Login";
import Register from "./Screens/Register/Register";
import Del_Add from "./Screens/Checkout/address";
import Edit from "./Screens/Profile/edit_profile";
import Terms from "./Screens/Information/Terms";
import Guarantee from "./Screens/Information/Guarantee";
import Contact from "./Screens/Information/Contact";
import Privacy from "./Screens/Information/Privacy";
import Locate from "./Screens/Information/Locate";
import EditAdd from "./Screens/Address/Edit_add";
import HeaderCart from "./Screens/Cart/HeaderCart";
import Add from "./Screens/Address/Add_add";
import Route from "./PrivateRoute";
import RotateLoader from "react-spinners/RotateLoader";
import Placed from "./Screens/Checkout/Placed";
import ErrorNotFound from "./Components/Error/Error";
import {
  ToastsContainer,
  ToastsStore,
  ToastsContainerPosition
} from "react-toasts";

import { connect } from "react-redux";

const NotFound = () => <p>Sorry, nothing here</p>;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }
  render() {
    return (
      <>
        {this.state.loading && (
          <div id="load">
            <RotateLoader
              sizeUnit={"px"}
              size={15}
              color={"#123abc"}
              loading={true}
            />
          </div>
        )}
        <div className="App">
          <Router>
            <Header path={"/"}>
              <Corausel path="/" />
              <List path="Product_list" />
              <Details path="Pro_details/:product_id" />
              <Login path="Login" />
              <Route path="/">
                <Address path="Address" />
                <Profile path="Profile" />
                <Orders path="Orders" />
                <ErrorNotFound default />
              </Route>
              <Register path="Register" />
              <Edit path="edit_pro" />
              <Terms path="terms" />
              <Guarantee path="guarantee" />
              <Contact path="contact" />
              <Privacy path="privacy" />
              <Locate path="locate_us" />
              <EditAdd path="edit_add" />
              <Add path="/Add_add" />
              <Cart path="cart" />
              <ErrorNotFound default />
            </Header>
            <HeaderCart path="order">
              <Cart path="cart" />
              <Del_Add path="del_address" />
              <Placed path="/Placed" />
              <ErrorNotFound default />
            </HeaderCart>

            <ErrorNotFound default />
          </Router>
          <ToastsContainer
            position={ToastsContainerPosition.TOP_CENTER}
            store={ToastsStore}
          />
          <br />
          <br />
          <br />
          <br />
          <br />
          <br />
          <br />
          <br />
          <br />
          <br />
          <br />
          <br />
          <Footer />
          <br />
        </div>
      </>
    );
  }
}

export default connect(state => ({
  isLoading: state.CheckoutReducer.isLoading
}))(App);
