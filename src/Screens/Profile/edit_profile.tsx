import React, { Component } from "react";
import "./edit_profile.css";
import { Formik } from "formik";
import { Link } from "@reach/router";
import * as Yup from "yup";
import axios from "axios";
import { navigate } from "@reach/router";
import { File } from "../../../node_modules/@babel/types";

interface state {
  first_name: string;
  last_name: string;
  email: string;
  dob: string;
  phone: string;
  gender: string;
  //   profile_img: string | Blob;
}
class Edit extends Component<{}, state> {
  reload: any;
  profile: string;
  profileObject: any;
  constructor(props: any) {
    super(props);
    this.state = {
      first_name: "",
      last_name: "",
      email: "",
      dob: "",
      phone: "",
      gender: ""
    };
    this.profile = localStorage.getItem("data") || "";
    this.profileObject = JSON.parse(this.profile);
    console.log(this.profileObject);
  }
  render() {
    return (
      <div className="container">
        <div className="container">
          <div className="container-fluid">
            <div className="col-md-12">
              <h4> My Account </h4>
              <hr />
            </div>

            <div className="col-md-3">
              <img
                src={
                  "http://10.0.100.228:3000/" + this.profileObject.profile_img
                }
                className="img-responsive img-circle profile"
                alt="profile_pic"
              />

              <div className="text-center">
                <br />
                <div className="text-danger">
                  <strong>{this.profileObject.first_name}</strong>
                </div>
              </div>
              <br />
              <ul className="nav">
                <li>
                  <a href="/Orders">
                    <i className="fa fa-pencil fa-fw" aria-hidden="true" />
                    &nbsp; Orders
                  </a>
                </li>
                <li>
                  <a href="/Profile">
                    <i className="fa fa-user fa-fw" aria-hidden="true" />
                    &nbsp; Profile
                  </a>
                </li>
                <li>
                  <a href="/Address">
                    <i
                      className="fa fa-address-book fa-fw"
                      aria-hidden="true"
                    />
                    &nbsp; Address
                  </a>
                </li>
              </ul>
            </div>

            <div className="col-md-9">
              <div className="panel panel-default">
                <div className="panel-body">
                  <h4>
                    <strong>Edit Profile</strong>
                  </h4>
                  <hr />

                  <Formik
                    initialValues={{
                      first_name: this.profileObject.first_name,
                      last_name: this.profileObject.last_name,
                      email: this.profileObject.email,
                      dob: this.profileObject.dob,
                      phone: this.profileObject.phone,
                      gender: "",
                      profile_img: ""
                    }}
                    onSubmit={(values, { setSubmitting }) => {
                      let formdata = new FormData();
                      const {
                        first_name,
                        last_name,
                        email,
                        dob,
                        phone,
                        gender
                      } = this.state;
                      console.log("sdas", values.profile_img);
                      formdata.append("first_name", values.first_name);
                      formdata.append("last_name", values.last_name);
                      formdata.append("email", values.email);
                      formdata.append("dob", values.dob);
                      formdata.append("phone", values.phone);
                      formdata.append("gender", values.gender);
                      formdata.append("profile_img", values.profile_img);

                      const contentType = {
                        headers: {
                          Authorization: localStorage.getItem("token"),
                          "content-type": "multipart/form-data"
                        }
                      };

                      axios
                        .post(
                          "http://10.0.100.228:3000/editprofile",
                          formdata,
                          contentType
                        )
                        .then(response => {
                          console.log(response.data);
                          alert(response.data.message);
                          navigate("/Profile");
                          localStorage.setItem(
                            "data",
                            JSON.stringify(response.data.customer)
                          );
                        })
                        .catch(error => {
                          console.log(error);
                        });
                    }}
                    validationSchema={Yup.object().shape({
                      first_name: Yup.string().required("Required"),
                      last_name: Yup.string().required("Required"),
                      email: Yup.string()
                        .email()
                        .required("Required"),
                      phone: Yup.string()
                        .matches(/^[6-9]\d{9}$/, "Must be 10 digits")
                        .required("Required"),
                      gender: Yup.string().required("Required"),
                      dob: Yup.string().required("Required"),
                      profile_img: Yup.mixed().required("Required")
                    })}
                  >
                    {props => {
                      const {
                        values,
                        touched,
                        errors,
                        handleChange,
                        handleBlur,
                        setFieldValue,
                        handleSubmit
                      } = props;
                      return (
                        <form onSubmit={handleSubmit}>
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>First Name</label>
                                <input
                                  type="text"
                                  className={
                                    errors.first_name && touched.first_name
                                      ? "form-control error"
                                      : "form-control"
                                  }
                                  placeholder="First Name"
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  name="first_name"
                                  value={values.first_name}
                                />
                              </div>
                              <small className="text-danger">
                                {errors.first_name && touched.first_name && (
                                  <div className="input-feedback">
                                    {errors.first_name}
                                  </div>
                                )}
                              </small>
                            </div>
                          </div>

                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Last Name</label>
                                <input
                                  type="text"
                                  className={
                                    errors.last_name && touched.last_name
                                      ? "form-control error"
                                      : "form-control"
                                  }
                                  placeholder="Last Name"
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  name="last_name"
                                  value={values.last_name}
                                />
                              </div>
                              <small className="text-danger">
                                {errors.last_name && touched.last_name && (
                                  <div className="input-feedback">
                                    {errors.last_name}
                                  </div>
                                )}
                              </small>
                            </div>
                          </div>

                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Gender</label>
                                <div className="checkbox">
                                  <label>
                                    <input
                                      //   className={
                                      //     errors.gender && touched.gender
                                      //       ? "form-control error"
                                      //       : "form-control"
                                      //   }
                                      type="radio"
                                      value={0}
                                      name="gender"
                                      onChange={handleChange}
                                      onBlur={handleBlur}
                                    />
                                    <strong>Male</strong>{" "}
                                  </label>
                                  <label>
                                    <input
                                      //   className={
                                      //     errors.gender && touched.gender
                                      //       ? "form-control error"
                                      //       : "form-control"
                                      //   }
                                      type="radio"
                                      value={1}
                                      name="gender"
                                      onChange={handleChange}
                                      onBlur={handleBlur}
                                    />
                                    <strong>Female</strong>{" "}
                                  </label>
                                </div>
                              </div>
                              <small className="text-danger">
                                {errors.gender && touched.gender && (
                                  <div className="input-feedback">
                                    {errors.gender}
                                  </div>
                                )}
                              </small>
                            </div>
                          </div>

                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Date of Birth</label>
                                <input
                                  type="date"
                                  className={
                                    errors.dob && touched.dob
                                      ? "form-control error"
                                      : "form-control"
                                  }
                                  value={values.dob}
                                  name="dob"
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                />
                              </div>
                              <small className="text-danger">
                                {errors.dob && touched.dob && (
                                  <div className="input-feedback">
                                    {errors.dob}
                                  </div>
                                )}
                              </small>
                            </div>
                          </div>

                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Mobile</label>
                                <input
                                  type="number"
                                  className={
                                    errors.phone && touched.phone
                                      ? "form-control error"
                                      : "form-control"
                                  }
                                  placeholder="Enter Phone Number"
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  name="phone"
                                  value={values.phone}
                                />
                              </div>
                              <small className="text-danger">
                                {errors.phone && touched.phone && (
                                  <div className="input-feedback">
                                    {errors.phone}
                                  </div>
                                )}
                              </small>
                            </div>
                          </div>

                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Email</label>
                                <input
                                  type="email"
                                  className={
                                    errors.email && touched.email
                                      ? "form-control error"
                                      : "form-control"
                                  }
                                  placeholder="Your Email Address"
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  name="email"
                                  value={values.email}
                                />
                              </div>
                              <small className="text-danger">
                                {errors.email && touched.email && (
                                  <div className="input-feedback">
                                    {errors.email}
                                  </div>
                                )}
                              </small>
                            </div>
                          </div>

                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Upload Profile Image</label>
                                <input
                                  type="file"
                                  className={
                                    errors.profile_img && touched.profile_img
                                      ? "form-control error"
                                      : "form-control"
                                  }
                                  placeholder="Your profile_img"
                                  onChange={(event: any) => {
                                    setFieldValue(
                                      "profile_img",
                                      event.currentTarget.files[0]
                                    );
                                  }}
                                  onBlur={handleBlur}
                                  name="profile_img"
                                />
                              </div>
                              <small className="text-danger">
                                {errors.profile_img && touched.profile_img && (
                                  <div className="input-feedback">
                                    {errors.profile_img}
                                  </div>
                                )}
                              </small>
                            </div>
                          </div>

                          <hr />
                          <button
                            type="submit"
                            className="btn btn-default btn-lg"
                          >
                            <i className="fa fa-floppy-o" />
                            Save
                          </button>
                          <Link
                            to="/Profile"
                            type="button"
                            className="btn btn-default btn-lg"
                          >
                            <i className="fa fa-remove" />
                            Cancel
                          </Link>
                        </form>
                      );
                    }}
                  </Formik>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Edit;
