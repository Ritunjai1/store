import React, { Component } from "react";
import "./Profile.css";
import { Link } from "@reach/router";
import axios from "axios";

class Profile extends Component {
  profile: string;
  profileObject: any;

  constructor(props: any) {
    super(props);

    this.profile = localStorage.getItem("data") || "";
    this.profileObject = JSON.parse(this.profile);
  }

  render() {
    return (
      <div className="container">
        <div className="container-fluid">
          <div className="col-md-12">
            <h4> My Account </h4>
            <hr />
          </div>

          <div className="col-md-3">
            <img
              src={"http://10.0.100.228:3000/" + this.profileObject.profile_img}
              className="img-responsive img-circle profile"
              alt="profile_pic"
            />

            <div className="text-center">
              <br />
              <div className="text-danger">
                <strong>{this.profileObject.first_name}</strong>
              </div>
            </div>
            <br />
            <ul className="nav">
              <li>
                <Link to="/Orders">
                  <i className="fa fa-pencil fa-fw" aria-hidden="true" />
                  &nbsp; Orders
                </Link>
              </li>
              <li>
                <Link to="#">
                  <i className="fa fa-user fa-fw" aria-hidden="true" />
                  &nbsp; Profile
                </Link>
              </li>
              <li>
                <Link to="/Address">
                  <i className="fa fa-address-book fa-fw" aria-hidden="true" />
                  &nbsp; Address
                </Link>
              </li>
            </ul>
          </div>

          <div className="col-md-9">
            <div className="panel panel-default">
              <div className="panel-heading">
                <h2>Profile</h2>
                <hr />
              </div>
              <div className="panel-body">
                <table className="table">
                  <tbody>
                    <tr>
                      <th>First Name</th>
                      <td>{this.profileObject.first_name}</td>
                    </tr>
                    <tr>
                      <th>Last Name</th>
                      <td>{this.profileObject.last_name}</td>
                    </tr>
                    <tr>
                      <th>Gender</th>
                      <td>
                        {this.profileObject.gender == 0 ? "Male" : "female"}
                      </td>
                    </tr>
                    <tr>
                      <th>Date of Birth</th>
                      <td>{this.profileObject.dob}</td>
                    </tr>
                    <tr>
                      <th>Mobile Number</th>
                      <td>{this.profileObject.phone}</td>
                    </tr>
                    <tr>
                      <th>Email</th>
                      <td>{this.profileObject.email}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className="panel-footer">
                <hr />
                <Link
                  to="/edit_pro"
                  type="button"
                  className="btn btn-default btn-lg"
                >
                  Edit
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Profile;
