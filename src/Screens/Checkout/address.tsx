import React, { Component } from "react";
import "./address.css";
import { Link } from "@reach/router";
import { connect } from "react-redux";
import axios from "axios";
import { bindActionCreators } from "redux";
import { cartDel } from "../../Redux/actions/cart";
import { ToastsContainer, ToastsStore } from "react-toasts";
import { initCheckout } from "../../Redux/actions";

interface IState {
  address?: string;
  cart_data: Object[];
  addressObject?: Object[];
  selected: string;
}

class Del_Add extends Component<any, IState> {
  address: string;
  addressObject: any;
  constructor(props: any) {
    super(props);
    this.state = {
      cart_data: [],
      selected: "",
      addressObject: []
    };
    this.address = localStorage.getItem("address") || "";
    this.addressObject = JSON.parse(this.address);
  }
  place = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    e.preventDefault();
    this.props.initCheckout({
      data: this.props.cart_data,
      address_id: this.state.selected,
      flag: true
    });
  };
  render() {
    return (
      <div>
        <div className="container">
          <div className="panel panel-default">
            <div className="panel-heading">
              <h3>Select delivery address</h3>
              <hr />
            </div>
            {this.addressObject.map((item: any, index: any) => (
              <div className="panel-body">
                <div className="panel panel-default">
                  <div className="panel-body" />
                  <div className="panel-footer">
                    <p>{item.address}</p>
                    <p>
                      {item.city}-{item.pincode}
                    </p>
                    <p>{item.state}</p>
                    <p>{item.country}</p>
                    <input
                      type="radio"
                      name={item._id}
                      checked={this.state.selected === item._id}
                      onChange={() => {
                        this.setState({
                          selected: item._id
                        });
                      }}
                    />{" "}
                    Select
                    <Link to="/edit_add" type="button" className=" btn">
                      <i className="fa fa-pencil" /> Edit
                    </Link>
                  </div>
                </div>
              </div>
            ))}
            {this.state.selected ? (
              <div className="panel-footer">
                <hr />
                <Link
                  to="/order/Placed"
                  type="button"
                  className="btn btn-default btn-lg"
                  onClick={e => this.place(e)}
                >
                  {" "}
                  Next <i className="fa fa-angle-double-right" />
                </Link>
                <Link
                  to="/Add_add"
                  type="button"
                  className="btn btn-default btn-lg"
                >
                  {" "}
                  Add New <i className="fa fa-plus" />
                </Link>
              </div>
            ) : (
              <div className="panel-footer">
                <hr />
                <div />
                <Link
                  to="/Add_add"
                  type="button"
                  className="btn btn-default btn-lg"
                >
                  {" "}
                  Add New <i className="fa fa-plus" />
                </Link>
              </div>
            )}
          </div>
        </div>

        <div className="footer">
          <div className="container">
            <hr />
            <div className="row text-center">
              <i className="text-info fa fa-3x fa-cc-paypal " />
              <i className="text-danger fa fa-3x fa-cc-mastercard " />
              <i className="text-primary fa fa-3x fa-cc-visa " />
              <i className="text-primary fa fa-3x fa-cc-amex " />
              <i className="text-primary fa fa-3x fa-cc-discover " />
              <i className="text-info fa fa-3x fa-paypal " />
              <i className="text-danger fa fa-3x fa-google-wallet " />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default connect(
  (state: any) => ({
    cart_data: state.CartDataReducer,
    checkout: state.CheckoutReducer
  }),
  dispatch => bindActionCreators({ initCheckout }, dispatch)
)(Del_Add);
