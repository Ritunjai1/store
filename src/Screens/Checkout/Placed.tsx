import React, { Component } from "react";
import { Link } from "@reach/router";
import styled from "styled-components";
import { connect } from "react-redux";

const Div = styled.div`
  text-align: -webkit-center;
`;
class Placed extends Component<{ UserDataReducer: any }> {
  constructor(props: any) {
    super(props);
  }
  render() {
    let login = this.props.UserDataReducer.isLoggedIn;
    return (
      <div>
        <Div id="error">
          <h1 className="notFoundTitle">Thank you for shopping with us</h1>
          <p className="notFoundDesc">
            <Link to="/">Please Give us a chance to serve you more</Link>
          </p>
        </Div>
      </div>
    );
  }
}
export default connect(state => state)(Placed);
