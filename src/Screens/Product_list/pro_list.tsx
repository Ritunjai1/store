import React, { Component } from "react";
import "./pro_list.css";
import { Link } from "@reach/router";
import axios from "axios";
import Rating from "react-rating";
import { connect } from "react-redux";
import Pagination from "react-js-pagination";

interface IState {
  products: Object[];
  starRating: string;
  color: Object[];
  getCategory: string;
  category_id: any;
  color_id: any;
  category_name: string;
  product_id: string;
  activePage: number;
  pageNumber: number;
}
interface IProps {
  categories: Object[];
  cartData: Object[];
  location: any;
  dispatch: any;
  product_id: string;
  activePage: number;
  pageNumber: number;
  category_name: string;
}
class List extends Component<IProps, IState> {
  constructor(props: any) {
    super(props);
    this.state = {
      category_id: "",
      products: [],
      starRating: "",
      color: [],
      getCategory: "",
      color_id: "",
      category_name: "All Categories",
      product_id: "",
      activePage: 1,
      pageNumber: 1
    };
  }
  handlePageChange = (pageNumber: any) => {
    this.getProducts({
      limit: 6,
      page: pageNumber
    });
    console.log(`active page is ${pageNumber}`);
    this.setState({ activePage: pageNumber });
  };
  getProducts = (filter: any = {}) => {
    let tail = "";
    Object.keys(filter).map(key => {
      if (filter[key]) tail = tail + `${key}=${filter[key]}&`;
    });
    axios
      .get("http://10.0.100.228:3000/findproducts?" + tail)
      .then(response => {
        console.log(response.data.product);
        this.setState({
          products: response.data.product
        });
      })
      .catch(error => {
        console.log(error);
      });
  };
  starRating = (e: any) => {
    this.getProducts({
      sortBy: "product_rating",
      desc: "true",
      category_id: this.state.category_id,
      limit: 6,
      page: this.state.pageNumber
    });
  };
  maxPrice = (e: any) => {
    this.getProducts({
      sortBy: "product_cost",
      desc: "true",
      category_id: this.state.category_id,
      limit: 6,
      page: this.state.pageNumber
    });
  };
  minPrice = (e: any) => {
    this.getProducts({
      sortBy: "product_cost",
      desc: "false",
      category_id: this.state.category_id,
      limit: 6,
      page: this.state.pageNumber
    });
  };

  componentDidMount() {
    const { id } = this.props.location.state;

    this.getProducts({ category_id: id, limit: 6, page: 1 });

    axios
      .get("http://10.0.100.228:3000/colors")
      .then(response => {
        console.log(response);
        this.setState({
          color: response.data.colors
        });
      })
      .catch(error => {
        console.log(error);
      });
  }
  addCart = (item: any) => {
    {
      this.props.dispatch({
        type: "CART_DATA",
        payload: item
      });
    }
  };
  render() {
    console.log(this.state.activePage);
    const isOnCart =
      this.props.cartData.findIndex(
        (product: any) => this.props.product_id === product._id
      ) > -1;
    console.log(this.props.cartData);
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-3">
            <div
              className="panel-group"
              id="accordion"
              role="tablist"
              aria-multiselectable="true"
            >
              <div className="panel panel-danger">
                <div className="panel-heading" role="tab" id="headingOne">
                  <h4 className="panel-title">
                    <a
                      data-toggle="collapse"
                      data-parent="#accordion"
                      href="#categories"
                    >
                      <i className="fa fa-lg fa-angle-double-down" /> Categories
                    </a>
                  </h4>
                </div>

                <div
                  id="categories"
                  className="panel-collapse collapse in"
                  role="tabpanel"
                  aria-labelledby="headingOne"
                >
                  {" "}
                  <div className="list-group">
                    {this.props.categories.map((item: any, index: any) => (
                      <li
                        className="list-group-item"
                        onClick={(e: any) => {
                          this.getProducts({
                            category_id: item._id
                          });
                          this.setState({
                            category_id: item._id,
                            category_name: item.category_name
                          });
                        }}
                      >
                        <Link to="/Product_list">
                          <i className="fa fa-dot-circle-o" />{" "}
                          {item.category_name}
                        </Link>
                      </li>
                    ))}
                  </div>
                </div>
              </div>

              <div className="panel panel-danger">
                <div className="panel-heading" role="tab" id="headingOne">
                  <h4 className="panel-title">
                    <a
                      data-toggle="collapse"
                      data-parent="#accordion"
                      href="#colors"
                    >
                      <i className="fa fa-lg fa-angle-double-down" /> Colors
                    </a>
                  </h4>
                </div>
                <div
                  id="colors"
                  className="panel-collapse collapse in"
                  role="tabpanel"
                  aria-labelledby="headingOne"
                >
                  <div className="list-group-item">
                    <ul className="list-inline">
                      {this.state.color.map((item: any, index: any) => (
                        <li>
                          <button
                            type="button"
                            data-toggle="tooltip"
                            data-placement="top"
                            title="color"
                            className="color-box"
                            style={{ backgroundColor: item.color_code }}
                            onClick={(e: any) => {
                              this.getProducts({
                                color_id: item.color_id
                              });
                              this.setState({
                                color_id: item.color_id
                              });
                            }}
                          />
                        </li>
                      ))}
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="col-md-9 vertical-line">
            <div className="row padding-row">
              <h5 className="pull-left">
                {this.state.category_name}
                {/* <Link to="/">All Categories</Link> */}
              </h5>

              <div className="pull-right">
                <ul className="nav nav-pills" role="tablist">
                  <li className="nav-item active">
                    <a
                      className="nav-link"
                      href="#tab1"
                      aria-controls="tab1"
                      role="tab"
                      data-toggle="tab"
                      onClick={this.starRating}
                    >
                      <i className="fa fa-star" aria-hidden="true" />
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className="nav-link"
                      href="#tab2"
                      aria-controls="tab2"
                      role="tab"
                      data-toggle="tab"
                      onClick={this.maxPrice}
                    >
                      <i className="fa fa-inr" aria-hidden="true" />
                      <i className="fa fa-arrow-up" aria-hidden="true" />
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className="nav-link"
                      href="#tab3"
                      aria-controls="tab3"
                      role="tab"
                      data-toggle="tab"
                      onClick={this.minPrice}
                    >
                      <i className="fa fa-inr" aria-hidden="true" />
                      <i className="fa fa-arrow-down" aria-hidden="true" />
                    </a>
                  </li>
                </ul>
              </div>
              <h5 className="pull-right">Sort By : </h5>
            </div>

            <br />
            {this.state.products.map((item: any, index: any) => (
              <div className="col-md-4" id="product-card" key={item._id}>
                <div className="thumbnail">
                  <div className="img-thumb">
                    <img
                      className="img-reposive"
                      src={"http://10.0.100.228:3000/" + item.product_image[0]}
                      alt="product_image"
                    />
                  </div>
                  <div className="caption">
                    <p className="elipse-product">
                      <Link to={`/Pro_details/${item._id}`}>
                        {item.product_name}
                      </Link>
                    </p>
                    <button
                      className="pull-right btn btn-danger btn-xs"
                      onClick={() => {
                        this.addCart(item);
                      }}
                      disabled={isOnCart}
                    >
                      Add To Cart
                    </button>
                    <p>
                      <strong>₹{item.product_cost}</strong>
                    </p>
                    <div className="Rating">
                      <Rating
                        className="fa-2x"
                        emptySymbol="fa fa-star-o fa-2x"
                        fullSymbol="fa fa-star fa-2x"
                        fractions={2}
                        readonly={true}
                        initialRating={item.product_rating}
                      />
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
        <Pagination
          activePage={this.state.activePage}
          itemsCountPerPage={6}
          totalItemsCount={27}
          pageRangeDisplayed={6}
          onChange={this.handlePageChange}
        />
      </div>
    );
  }
}
export default connect((state: any) => ({
  categories: state.CategoryDataReducer,
  cartData: state.CartDataReducer
}))(List);
