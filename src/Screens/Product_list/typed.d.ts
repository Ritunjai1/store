export interface ProductType {
  product_name?: string;
  product_cost?: string;
  product_image?: string[];
  prod_desc?: string;
  product_dimension?: string;
  key?: string;
  product_rating?: number;
  color_id?: Color;
  product_material?: string;
  product_producer?: string;
}

interface Color {
  _id: string;
  color_code: string;
  color_name: string;
  color_id: string;
}
