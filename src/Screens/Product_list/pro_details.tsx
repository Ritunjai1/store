import React, { Component } from "react";
import "./pro_details.css";
import axios from "axios";
import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  WhatsappShareButton
} from "react-share";
import { ProductType } from "./typed";
import Rating from "react-rating";
import Magnifier from "react-magnifier";
import { connect } from "react-redux";
import { Modal } from "react-bootstrap";
// import ErrorNotFound from "../../Components/Error/Error";
import { navigate } from "@reach/router";


interface props {
  product_id: string;
  dispatch: any;
  cart_data: Object[];
  cartData: any;
}
interface IState {
  product: ProductType;
  isSelected: boolean;
  previewImage: Object[];
  color_code: string;
  cart_data: Object[];
  ratingModal: boolean;
}

class Details extends Component<props, IState> {
  constructor(props: any) {
    super(props);
    this.state = {
      product: {},
      isSelected: true,
      previewImage: [],
      color_code: "",
      cart_data: [],
      ratingModal: false
    };
  }
  componentDidMount() {
    console.log(this.props);
    axios
      .post("http://10.0.100.228:3000/findproduct", {
        _id: this.props.product_id
      })
      .then(response => {
        console.log(response.data.product[0]);
        this.setState({
          product: response.data.product[0],
          previewImage: response.data.product[0].product_image[0]
        });
      })
      .catch(error => {
        navigate("/ErrorNotFound");
      });
  }
  change = (item: any) => {
    this.setState({
      previewImage: item
    });
  };
  cart = () => {
    this.props.dispatch({
      type: "CART_DATA",
      payload: this.state.product
    });
  };
  Rate = () => {};

  handleModal = () => {
    this.setState({
      ratingModal: !this.state.ratingModal
    });
  };

  render() {
    const isInCart =
      this.props.cartData.findIndex(
        (product: any) => this.props.product_id === product._id
      ) > -1;

    return (
      <div className="container">
        <div className="card">
          <div className="row">
            <div className="wrapper">
              <div className="col-md-6">
                <div className="preview">
                  <div className="preview-pic tab-content">
                    <div className="my-img active">
                      <Magnifier
                        className="actual-img"
                        src={
                          "http://10.0.100.228:3000/" + this.state.previewImage
                        }
                        mgShape="square"
                        zoomFactor={1.5}
                        mgWidth={250}
                      />
                    </div>
                  </div>
                  <ul className="preview-thumbnail nav nav-tabs">
                    {this.state.product.product_image &&
                      this.state.product.product_image.map(
                        (item: any, index: any) => (
                          <li key={item._id}>
                            <div
                              key={item._id}
                              className="my-img-thumb"
                              onClick={() => this.change(item)}
                            >
                              <img src={"http://10.0.100.228:3000/" + item} />
                            </div>
                          </li>
                        )
                      )}
                  </ul>
                </div>
              </div>

              <div className="col-md-6 ">
                <div className="details">
                  <h3 className="text-danger">
                    {this.state.product.product_name}
                  </h3>

                  <Rating
                    emptySymbol="fa fa-star-o fa-2x"
                    fullSymbol="fa fa-star fa-2x"
                    fractions={2}
                    readonly={true}
                    initialRating={this.state.product.product_rating}
                  />
                  <hr />
                  <h4>
                    Price:{" "}
                    <span className="text-success">
                      ₹{this.state.product.product_cost}
                    </span>
                  </h4>
                  <h4>
                    Color:
                    <button
                      className="color"
                      style={{
                        backgroundColor: this.state.product.color_id
                          ? this.state.product.color_id.color_code
                          : "red"
                      }}
                    />
                  </h4>

                  <div className="action">
                    <h4>
                      Share on
                      <i className="fa fa-share-alt fa-lg" />
                    </h4>
                    <div className="share-container">
                      <FacebookShareButton
                        url={"Pro_details/5d03491e4bf6f04a127abe74"}
                      >
                        <button className="btn btn-primary">
                          <i className="fa fa-lg fa-facebook" />
                        </button>
                      </FacebookShareButton>
                      &nbsp;
                      <TwitterShareButton url={"https://twitter.com"}>
                        <button className="btn btn-info">
                          <i className="fa fa-lg fa-twitter" />
                        </button>
                      </TwitterShareButton>
                      &nbsp;
                      <LinkedinShareButton url={"https://www.linkedin.com"}>
                        <button className="btn btn-primary">
                          <i className="fa fa-lg fa-linkedin" />
                        </button>
                      </LinkedinShareButton>
                      &nbsp;
                      <WhatsappShareButton url={"https://web.whatsapp.com/"}>
                        <button className="btn btn-success">
                          <i className="fa fa-lg fa-whatsapp" />
                        </button>
                      </WhatsappShareButton>
                    </div>
                  </div>

                  <div className="action">
                    <button
                      onClick={() => {
                        this.cart();
                      }}
                      className="btn btn-primary"
                      type="button"
                      id="cartBtn"
                      disabled={isInCart}
                    >
                      ADD TO CART
                    </button>
                    <button
                      type="button"
                      className="btn btn-warning"
                      data-toggle="modal"
                      onClick={this.handleModal}
                      data-target=".bs-example-modal-sm"
                    >
                      RATE PRODUCT
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <hr />
            <div className="wrapper">
              <div className="col-md-12">
                <div className="preview">
                  <div>
                    <ul className="nav nav-tabs" role="tablist">
                      <li role="presentation" className="active">
                        <a
                          href="#tab1"
                          aria-controls="tab1"
                          role="tab"
                          data-toggle="tab"
                        >
                          Description
                        </a>
                      </li>
                      <li role="presentation">
                        <a
                          href="#tab2"
                          aria-controls="tab2"
                          role="tab"
                          data-toggle="tab"
                        >
                          Features
                        </a>
                      </li>
                    </ul>
                    <br />

                    <div className="tab-content">
                      <div
                        role="tabpanel"
                        className="tab-pane active"
                        id="tab1"
                      >
                        {this.state.product.prod_desc}
                      </div>

                      <div role="tabpanel" className="tab-pane" id="tab2">
                        {this.state.product.product_dimension}
                        <br />
                        {this.state.product.product_material}
                        <br />
                        {this.state.product.product_producer}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Modal show={this.state.ratingModal} onHide={this.handleModal}>
          <Modal.Header closeButton>
            <Modal.Title>Modal heading</Modal.Title>
          </Modal.Header>
        </Modal>
        <div
          className="modal fade bs-example-modal-sm "
          role="dialog"
          tabIndex={-1}
          aria-labelledby="mySmallModalLabel"
        >
          <div className="modal-dialog modal-sm" role="document">
            <div className="modal-content">
              <div className="modal-body">
                <div className="text-warning text-center">
                  <i className="fa fa-lg fa-star" />
                  <i className="fa fa-lg fa-star" />
                  <i className="fa fa-lg fa-star" />
                  <i className="fa fa-lg fa-star-o" />
                  <i className="fa fa-lg fa-star-o" />
                </div>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-default"
                  data-dismiss="modal"
                >
                  CLOSE
                </button>
                <button type="button" className="btn btn-primary">
                  RATE IT
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default connect((state: any) => ({
  cartData: state.CartDataReducer
}))(Details);
