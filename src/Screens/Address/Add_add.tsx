import React, { Component } from "react";
import "./Address.css";
import { Link } from "@reach/router";
import axios from "axios";
import { Formik } from "formik";
import * as Yup from "yup";
import { navigate } from "@reach/router";
import { ToastsStore } from "react-toasts";

class Add extends Component {
  profile: string;
  profileObject: any;
  address: string;
  addressObject: any;
  constructor(props: any) {
    super(props);
    this.profile = localStorage.getItem("data") || "";
    this.profileObject = JSON.parse(this.profile);
    this.address = localStorage.getItem("address") || "";
    this.addressObject = JSON.parse(this.address);
    console.log(this.addressObject);
  }

  render() {
    return (
      <div className="container">
        <div className="container">
          <div className="container-fluid">
            <div className="col-md-12">
              <h4> My Account </h4>
              <hr />
            </div>

            <div className="sidebar col-md-3">
              <p>
                <small>ORDERS</small>
              </p>
              <p>
                <Link to="/Orders">Orders</Link>
              </p>
              <hr />
              <p>
                <small>ACCOUNT</small>
              </p>
              <p>
                <Link to="/Profile">Profile</Link>
              </p>
              <p>
                <Link to="/Address">Address</Link>
              </p>
            </div>

            <div className="col-md-9">
              <div className="panel panel-default">
                <div className="panel-body">
                  <h4>
                    <strong>Add new address</strong>
                  </h4>
                  <hr />

                  <button type="button" className="btn btn-default">
                    <i className="fa fa-map-marker" /> Set Current Location
                  </button>
                  <br />
                  <br />
                  <Formik
                    initialValues={{
                      address: "",
                      pincode: "",
                      city: "",
                      state: "",
                      country: ""
                    }}
                    onSubmit={(values, { setSubmitting }) => {
                      axios
                        .post("http://10.0.100.228:3000/address", values, {
                          headers: {
                            Authorization: localStorage.getItem("token")
                          }
                        })
                        .then(response => {
                          console.log(response.data.user);
                          this.addressObject.push(response.data.user);
                          console.log(this.addressObject);
                          localStorage.setItem(
                            "address",
                            JSON.stringify(this.addressObject)
                          );
                          ToastsStore.success("Added successfully");
                          navigate("/Address");
                        })
                        .catch(error => {
                          console.log(error);
                        });
                    }}
                    validationSchema={Yup.object().shape({
                      address: Yup.string().required("Required"),
                      pincode: Yup.string()
                        .matches(/^[0-9]{6}$/, "Must be six digits")
                        .required("Required"),
                      city: Yup.string().required("Please enter valid city"),
                      state: Yup.string().required("Please enter valid state"),
                      country: Yup.string().required(
                        "Please enter valid country"
                      )
                    })}
                  >
                    {props => {
                      const {
                        values,
                        touched,
                        errors,
                        handleChange,
                        handleBlur,
                        handleSubmit
                      } = props;
                      return (
                        <form className="form-custom" onSubmit={handleSubmit}>
                          <div className="row">
                            <div className="col-md-8">
                              <div className="form-group">
                                <label>Address</label>
                                <textarea
                                  className={
                                    errors.address && touched.address
                                      ? "form-control error"
                                      : "form-control"
                                  }
                                  name="address"
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  value={values.address}
                                />
                              </div>
                              <small className="text-danger">
                                {errors.address && touched.address && (
                                  <div className="input-feedback">
                                    {errors.address}
                                  </div>
                                )}
                              </small>
                            </div>
                          </div>

                          <div className="row">
                            <div className="col-md-4">
                              <div className="form-group">
                                <label>Pincode</label>
                                <input
                                  type="number"
                                  className={
                                    errors.pincode && touched.pincode
                                      ? "form-control error"
                                      : "form-control"
                                  }
                                  value={values.pincode}
                                  name="pincode"
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                />
                              </div>
                              <small className="text-danger">
                                {errors.pincode && touched.pincode && (
                                  <div className="input-feedback">
                                    {errors.pincode}
                                  </div>
                                )}
                              </small>
                            </div>
                          </div>

                          <div className="row">
                            <div className="col-md-4">
                              <div className="form-group">
                                <label>City</label>
                                <input
                                  type="text"
                                  className={
                                    errors.city && touched.city
                                      ? "form-control error"
                                      : "form-control"
                                  }
                                  value={values.city}
                                  name="city"
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                />
                              </div>
                              <small className="text-danger">
                                {errors.city && touched.city && (
                                  <div className="input-feedback">
                                    {errors.city}
                                  </div>
                                )}
                              </small>
                            </div>
                          </div>

                          <div className="row">
                            <div className="col-md-4">
                              <div className="form-group">
                                <label>State</label>
                                <input
                                  type="text"
                                  className={
                                    errors.state && touched.state
                                      ? "form-control error"
                                      : "form-control"
                                  }
                                  value={values.state}
                                  name="state"
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                />
                              </div>
                              <small className="text-danger">
                                {errors.state && touched.state && (
                                  <div className="input-feedback">
                                    {errors.state}
                                  </div>
                                )}
                              </small>
                            </div>
                          </div>

                          <div className="row">
                            <div className="col-md-4">
                              <div className="form-group">
                                <label>Country</label>
                                <input
                                  className={
                                    errors.country && touched.country
                                      ? "form-control error"
                                      : "form-control"
                                  }
                                  value={values.country}
                                  name="country"
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                />
                              </div>
                              <small className="text-danger">
                                {errors.country && touched.country && (
                                  <div className="input-feedback">
                                    {errors.country}
                                  </div>
                                )}
                              </small>
                            </div>
                          </div>

                          <hr />
                          <button
                            type="submit"
                            className="btn btn-default btn-lg"
                          >
                            <i className="fa fa-floppy-o" />
                            Save
                          </button>
                          <a
                            href="./address.html"
                            type="button"
                            className="btn btn-default btn-lg"
                          >
                            <i className="fa fa-remove" />
                            Cancel
                          </a>
                        </form>
                      );
                    }}
                  </Formik>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Add;
