import React, { Component } from "react";
import "./Edit_add.css";
import { Formik } from "formik";
import * as Yup from "yup";
import axios from "axios";
import { Link } from "@reach/router";
import { navigate } from "@reach/router";

class EditAdd extends Component<any> {
  profile: string;
  profileObject: any;
  address: string;
  addressObject: any;
  constructor(props: any) {
    super(props);
    this.profile = localStorage.getItem("data") || "";
    this.profileObject = JSON.parse(this.profile);
    this.address = localStorage.getItem("address") || "";
    this.addressObject = JSON.parse(this.address);
    console.log(this.addressObject);
  }
  render() {
    let a = this.props.location.state;
    console.log(a);
    return (
      <div>
        <div className="container">
          <div className="container">
            <div className="container-fluid">
              <div className="col-md-12">
                <h4> My Account </h4>
                <hr />
              </div>

              <div className="col-md-3">
                <img
                  src={
                    "http://10.0.100.228:3000/" + this.profileObject.profile_img
                  }
                  className="img-responsive img-circle profile"
                  alt="profile_pic"
                />

                <div className="text-center">
                  <br />
                  <div className="text-danger">
                    <strong>{this.profileObject.first_name}</strong>
                  </div>
                </div>
                <br />
                <ul className="nav">
                  <li>
                    <a href="/Orders">
                      <i className="fa fa-pencil fa-fw" aria-hidden="true" />
                      &nbsp; Orders
                    </a>
                  </li>
                  <li>
                    <a href="/Profile">
                      <i className="fa fa-user fa-fw" aria-hidden="true" />
                      &nbsp; Profile
                    </a>
                  </li>
                  <li>
                    <a href="/Address">
                      <i
                        className="fa fa-address-book fa-fw"
                        aria-hidden="true"
                      />
                      &nbsp; Address
                    </a>
                  </li>
                </ul>
              </div>

              <div className="col-md-9">
                <div className="panel panel-default">
                  <div className="panel-body">
                    <h4>
                      <strong>Add new address</strong>
                    </h4>
                    <hr />

                    <button type="button" className="btn btn-default">
                      <i className="fa fa-map-marker" /> Set Current Location
                    </button>
                    <br />
                    <br />
                    <Formik
                      initialValues={{
                        address: a.address,
                        pincode: a.pincode,
                        city: a.city,
                        state: a.state,
                        country: a.country,
                        address_id: a._id
                      }}
                      onSubmit={(values, { setSubmitting }) => {
                        axios
                          .post(
                            "http://10.0.100.228:3000/editaddress",
                            values,
                            {
                              headers: {
                                Authorization: localStorage.getItem("token")
                              }
                            }
                          )
                          .then(response => {
                            console.log(response.data.address);
                            alert("Updated successfully");
                            navigate("/Address");
                            const result = this.addressObject.map(
                              (item: any, index: any) => {
                                if (item._id === response.data.address[0]._id) {
                                  return response.data.address[0];
                                }
                                return item;
                              }
                            );
                            localStorage.setItem(
                              "address",
                              JSON.stringify(result)
                            );
                          })
                          .catch(error => {
                            console.log(error);
                          });
                      }}
                      validationSchema={Yup.object().shape({
                        address: Yup.string().required("Required"),
                        pincode: Yup.string()
                          .matches(/^[0-9]{6}$/, "Must be six digits")
                          .required("Required"),
                        city: Yup.string().required("Please enter valid city"),
                        state: Yup.string().required(
                          "Please enter valid state"
                        ),
                        country: Yup.string().required(
                          "Please enter valid country"
                        )
                      })}
                    >
                      {props => {
                        const {
                          values,
                          touched,
                          errors,
                          handleChange,
                          handleBlur,
                          handleSubmit
                        } = props;
                        return (
                          <form onSubmit={handleSubmit}>
                            <div className="row">
                              <div className="col-md-8">
                                <div className="form-group">
                                  <label>Address</label>
                                  <textarea
                                    className={
                                      errors.address && touched.address
                                        ? "form-control error"
                                        : "form-control"
                                    }
                                    name="address"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.address}
                                  />
                                </div>
                                <small className="text-danger">
                                  {errors.address && touched.address && (
                                    <div className="input-feedback">
                                      {errors.address}
                                    </div>
                                  )}
                                </small>
                              </div>
                            </div>

                            <div className="row">
                              <div className="col-md-4">
                                <div className="form-group">
                                  <label>pincode</label>
                                  <input
                                    type="number"
                                    className={
                                      errors.pincode && touched.pincode
                                        ? "form-control error"
                                        : "form-control"
                                    }
                                    value={values.pincode}
                                    name="pincode"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                  />
                                </div>
                                <small className="text-danger">
                                  {errors.pincode && touched.pincode && (
                                    <div className="input-feedback">
                                      {errors.pincode}
                                    </div>
                                  )}
                                </small>
                              </div>
                            </div>

                            <div className="row">
                              <div className="col-md-4">
                                <div className="form-group">
                                  <label>city</label>
                                  <input
                                    type="text"
                                    className={
                                      errors.city && touched.city
                                        ? "form-control error"
                                        : "form-control"
                                    }
                                    value={values.city}
                                    name="city"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                  />
                                </div>
                                <small className="text-danger">
                                  {errors.city && touched.city && (
                                    <div className="input-feedback">
                                      {errors.city}
                                    </div>
                                  )}
                                </small>
                              </div>
                            </div>

                            <div className="row">
                              <div className="col-md-4">
                                <div className="form-group">
                                  <label>state</label>
                                  <input
                                    type="text"
                                    className={
                                      errors.state && touched.state
                                        ? "form-control error"
                                        : "form-control"
                                    }
                                    value={values.state}
                                    name="state"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                  />
                                </div>
                                <small className="text-danger">
                                  {errors.state && touched.state && (
                                    <div className="input-feedback">
                                      {errors.state}
                                    </div>
                                  )}
                                </small>
                              </div>
                            </div>

                            <div className="row">
                              <div className="col-md-4">
                                <div className="form-group">
                                  <label>country</label>
                                  <input
                                    className={
                                      errors.country && touched.country
                                        ? "form-control error"
                                        : "form-control"
                                    }
                                    value={values.country}
                                    name="country"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                  />
                                </div>
                                <small className="text-danger">
                                  {errors.country && touched.country && (
                                    <div className="input-feedback">
                                      {errors.country}
                                    </div>
                                  )}
                                </small>
                              </div>
                            </div>

                            <hr />
                            <button
                              type="submit"
                              className="btn btn-default btn-lg"
                            >
                              <i className="fa fa-floppy-o" />
                              Save
                            </button>
                            <Link
                              to="/Address"
                              type="button"
                              className="btn btn-default btn-lg"
                            >
                              <i className="fa fa-remove" />
                              Cancel
                            </Link>
                          </form>
                        );
                      }}
                    </Formik>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default EditAdd;
