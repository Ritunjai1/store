import React, { Component } from "react";
import "./Address.css";
import { Link } from "@reach/router";
import axios from "axios";
import { ToastsStore } from "react-toasts";

class Address extends Component {
  profile: string;
  profileObject: any;
  address: string;
  addressObject: any;
  constructor(props: any) {
    super(props);
    this.profile = localStorage.getItem("data") || "";
    this.profileObject = JSON.parse(this.profile);
    this.address = localStorage.getItem("address") || "";
    this.addressObject = JSON.parse(this.address);
    console.log(this.addressObject);
    // this.state = {
    //   address: this.addressObject
    // };
  }
  delete = (id: any) => {
    console.log(this.addressObject);
    let item = this.addressObject.filter((item: any) => {
      return id !== item._id;
    });
    console.log(item);
    axios
      .delete("http://10.0.100.228:3000/deleteaddress/" + id, {
        headers: {
          Authorization: localStorage.getItem("token")
        }
      })
      .then(response => {
        ToastsStore.success("Deleted successfully");
        localStorage.setItem("address", JSON.stringify(item));
        window.location.reload();
        console.log(response);
      })
      .catch(error => {
        console.log(error);
      });
  };
  render() {
    return (
      <div className="container">
        <div className="container">
          <div className="container-fluid">
            <div className="col-md-12">
              <h4> My Account </h4>
              <hr />
            </div>

            <div className="col-md-3">
              <img
                src={
                  "http://10.0.100.228:3000/" + this.profileObject.profile_img
                }
                className="img-responsive img-circle profile"
                alt="profile_pic"
              />

              <div className="text-center">
                <br />
                <div className="text-danger">
                  <strong>{this.profileObject.first_name}</strong>
                </div>
              </div>
              <br />
              <ul className="nav">
                <li>
                  <Link to="/Orders">
                    <i className="fa fa-pencil fa-fw" aria-hidden="true" />
                    &nbsp; Orders
                  </Link>
                </li>
                <li>
                  <Link to="/Profile">
                    <i className="fa fa-user fa-fw" aria-hidden="true" />
                    &nbsp; Profile
                  </Link>
                </li>
                <li>
                  <Link to="#">
                    <i
                      className="fa fa-address-book fa-fw"
                      aria-hidden="true"
                    />
                    &nbsp; Address
                  </Link>
                </li>
              </ul>
            </div>

            <div className="col-md-9">
              <div className="panel panel-default">
                <div className="panel-heading">
                  <h3>Addresses</h3>
                  <hr />
                </div>
                <div className="panel-body">
                  {this.addressObject.map((item: any, index: any) => (
                    <div className="panel panel-default" key={item._id}>
                      <div className="panel-body">
                        <p>{item.address}</p>
                        <p>
                          {item.city}-{item.pincode}
                        </p>
                        <p>{item.state}</p>
                        <p>{item.country}</p>
                      </div>
                      <div className="panel-footer">
                        {/* <input type="radio" name="address" /> Select */}
                        <Link
                          state={this.addressObject[index]}
                          to="/edit_add"
                          type="button"
                          className="btn-default"
                        >
                          <i className="fa fa-pencil" /> Edit
                        </Link>
                        <Link
                          to={""}
                          type="button"
                          className="btn"
                          id="but"
                          onClick={() => this.delete(item._id)}
                        >
                          <i className="fa fa-trash" /> {"     "}
                          Delete
                        </Link>
                      </div>
                    </div>
                  ))}
                </div>
                <div className="panel-footer">
                  <hr />
                  <Link
                    to="/Add_add"
                    type="button"
                    className="btn btn-default btn-lg"
                  >
                    Add new
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Address;
