import React, { Component } from "react";
import "./Corausel.css";
import { Link } from "@reach/router";
import axios from "axios";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import Rating from "react-rating";
import { navigate } from "@reach/router";
import { connect } from "react-redux";

interface IState {
  product: Object[];
  cusotmer: Object[];
}
class Corausel extends Component<any, IState> {
  constructor(props: any) {
    super(props);
    this.state = {
      product: [],
      cusotmer: []
    };
  }

  componentDidMount() {
    axios
      .get("http://10.0.100.228:3000/allcategory")
      .then(response => {
        this.props.dispatch({
          type: "CATEGORY_DATA",
          payload: response.data.product
        });
        console.log(response.data);
        this.setState({
          product: response.data.product
        });
      })
      .catch(error => {
        console.log(error);
      });
    axios
      .get("http://10.0.100.228:3000/showproducts")
      .then(response => {
        console.log(response.data);
        this.setState({
          cusotmer: response.data.customer
        });
      })
      .catch(error => {
        console.log(error);
      });
  }
  imgClick(e: React.MouseEvent<HTMLAnchorElement, MouseEvent>, id: string) {
    e.preventDefault();
    navigate("Product_list", { state: { id } });
  }
  public render() {
    console.log(this.props);

    return (
      <div className="container-fluid">
        <div className="row">
          <Carousel
            showArrows={true}
            showThumbs={false}
            autoPlay={true}
            infiniteLoop={true}
          >
            {this.state.product.map((item: any, index: any) => (
              <a
                id="click"
                onClick={e => this.imgClick(e, item._id)}
                key={item._id}
              >
                <div>
                  {" "}
                  <img
                    className="cora-omg"
                    src={"http://10.0.100.228:3000/" + item.category_image}
                  />
                </div>
              </a>
            ))}
          </Carousel>
        </div>
        <div className="container">
          <div className="row">
            <div className="container">
              <h3 className="text-center">
                Popular Products{" "}
                <small>
                  <Link to="Product_list">--View all</Link>
                </small>
              </h3>
              <div className="row">
                {this.state.cusotmer.map((item: any, index: any) => (
                  <div className="col-md-4" key={item._id}>
                    <div className="thumbnail" id="card_style">
                      <div className="img-thumb">
                        <img
                          id="card"
                          src={
                            "http://10.0.100.228:3000/" +
                            item.products.product_image[0]
                          }
                          className="img img-responsive"
                        />
                      </div>
                      <div className="caption">
                        <h4 className="text-center">
                          <Link to={`Pro_details/${item.products._id}`}>
                            {item.products.product_name}
                          </Link>
                        </h4>
                        <h4 className="text-center">
                          ₹{item.products.product_cost}
                        </h4>
                        <div className="Rating">
                          <Rating
                            emptySymbol="fa fa-star-o fa-2x"
                            fullSymbol="fa fa-star fa-2x"
                            fractions={2}
                            readonly={true}
                            initialRating={item.products.product_rating}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default connect((state: any) => ({
  data: state.CategoryDataReducer
}))(Corausel);
