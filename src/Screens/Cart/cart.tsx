import React, { Component } from "react";
import "../Cart/cart.css";
import { Link } from "@reach/router";
import axios from "axios";
import { connect } from "react-redux";
import { RemoveData, quantityInc, quantityDec } from "../../Redux/actions/cart";

import { bindActionCreators } from "redux"; // USe later

interface Istate {
  cart: Object[];
  cart_data: Object[];
  cartInc: number;
  cartDec: number;
  price: number;
  login: any;
}

class Cart extends Component<any, Istate> {
  constructor(props: any) {
    super(props);
    this.state = {
      cart: [],
      cart_data: [],
      cartInc: 0,
      cartDec: 0,
      price: 0,
      login: false
    };
  }
  cart = (value: any) => {
    console.log(value);
    this.props.dispatch({
      type: "CART_DATA",
      payload: value
    });
  };

  componentDidMount() {
    axios
      .get("http://10.0.100.228:3000/showcart", {
        headers: {
          Authorization: localStorage.getItem("token")
        }
      })
      .then(response => {
        this.cart(response.data.cart);
      })
      .catch(error => {
        console.log(error);
      });
  }
  delete = (item: any) => {
    console.log(item);
    this.props.removeCart(item);
  };
  incClick = (value: any, price: any) => {
    this.props.cartInc(value);
  };
  decClick = (value: any) => {
    this.props.cartDec(value);
  };
  // Reduce method for array
  subtotal = () => {
    const total = this.props.cart_data.reduce((acc: number, curValue: any) => {
      return acc + curValue.product_cost * curValue.qty;
    }, 0);

    return {
      total: total,
      gst: (total * 0.05).toFixed(2),
      subtotal: total + total * 0.05
    };
  };

  render() {
    //  = this.props.UserDataReducer.isLoggedIn;
    console.log(this.props.cart_data);
    let data = this.props.cart_data;
    console.log(data);
    let login = this.props.user_data.isLoggedIn;
    const { total, gst, subtotal } = this.subtotal();

    return (
      <div>
        <div className="container">
          {data.length > 0 ? (
            <div className="row">
              <hr />

              <div className="col-md-8">
                <table className="table table-hover">
                  <thead>
                    <tr>
                      <th>Product</th>
                      <th className="text-center">Quantity</th>
                      <th className="text-center">Price</th>
                      <th className="text-center">Total</th>
                      <th> </th>
                    </tr>
                  </thead>

                  <tbody>
                    {this.props.cart_data.map((item: any, index: any) => (
                      <tr key={item._id}>
                        <td className="col-sm-8 col-md-6">
                          <div className="media">
                            <div className="media-left">
                              <a>
                                <img
                                  className="media-object cover"
                                  src={
                                    "http://10.0.100.228:3000/" +
                                    item.product_image[0]
                                  }
                                />
                              </a>
                            </div>

                            <div className="media-body">
                              <h4 className="media-heading">
                                <a>{item.product_name}</a>
                              </h4>
                              <h5 className="media-heading">
                                {" "}
                                by {item.product_producer}
                              </h5>
                              <span>Status: </span>
                              <span className="text-success">
                                <strong>
                                  {item.product_stock == 0
                                    ? "Not in stock"
                                    : "In Stock"}
                                </strong>
                              </span>
                            </div>
                          </div>
                        </td>
                        <td className="col-sm-1 col-md-2 text-center">
                          <button
                            className="but"
                            onClick={() => this.decClick(item._id)}
                          >
                            <i className="fa fa-minus-circle" />
                          </button>
                          <input
                            type="number"
                            disabled
                            value={item.qty}
                            className="text-center quantity"
                          />
                          <button
                            className="but"
                            onClick={() =>
                              this.incClick(item._id, item.product_cost)
                            }
                          >
                            <i className="fa fa-plus-circle" />
                          </button>
                        </td>
                        <td className="col-sm-1 col-md-1 text-center">
                          <strong>₹{item.product_cost}</strong>
                        </td>
                        <td className="col-sm-1 col-md-1 text-center">
                          <strong>₹{item.product_cost * item.qty}</strong>
                        </td>
                        <td className="col-sm-1 col-md-1">
                          <button
                            type="button"
                            className="btn btn-sm btn-danger"
                            onClick={() => this.delete(item._id)}
                          >
                            <i className="fa fa-trash" />
                          </button>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>

              <div className="col-md-4">
                <div className="panel panel-default">
                  <div className="panel-heading text-center panel-heading-custom">
                    <h4>Review Order</h4>
                  </div>
                  <div className="panel-body">
                    <div className="col-md-12">
                      <strong>Subtotal</strong>
                      <div className="pull-right">
                        <span>₹{total}</span>
                      </div>
                      <hr />
                    </div>
                    <div className="col-md-12">
                      <strong>GST(5%)</strong>
                      <div className="pull-right">
                        <span>₹{gst}</span>
                      </div>
                      <hr />
                    </div>
                    <div className="col-md-12">
                      <strong>Order Total</strong>
                      <div className="pull-right">
                        <span>₹{subtotal}</span>
                      </div>
                      <hr />
                    </div>
                  </div>

                  <div className="panel-footer">
                    {login ? (
                      <Link
                        to="/order/del_address"
                        type="button"
                        className="btn btn-primary btn-lg btn-block"
                      >
                        Checkout
                      </Link>
                    ) : (
                      <Link
                        to="/Login"
                        type="button"
                        className="btn btn-primary btn-lg btn-block"
                      >
                        Checkout
                      </Link>
                    )}
                  </div>
                </div>
              </div>
            </div>
          ) : (
            <div>
              <h2 id="title">Your cart is empty</h2>
              <hr />
              <h3>
                Your Cart is lives to serve. Give it a purpose --fill it with
                Chairs, tables, wardrobes, cabinet and more
              </h3>
              <Link to="/">
                <h5>Continue shopping on the Neostore app</h5>
              </Link>
            </div>
          )}
        </div>
        <div />
      </div>
    );
  }
}

export default connect(
  // MapStateToProps => Redux Store =>  componennts this.props/props
  (state: any) => ({
    cart_data: state.CartDataReducer,
    user_data: state.UserDataReducer
  }),
  // MapDispatchToProps => map/add method/functions into the this.props of component
  dispatch => {
    // Solution 1
    return bindActionCreators(
      {
        removeCart: RemoveData,
        cartInc: quantityInc,
        cartDec: quantityDec
      },
      dispatch
    );

    // Solution 2
    // return {
    //   removeCart: (id: string) => {
    //     console.log(id);
    //   }
    // };
  }
)(Cart);
