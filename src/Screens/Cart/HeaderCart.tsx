import React, { Component } from "react";
import { Link } from "@reach/router";
import "../Cart/cart.css";
import "../../Components/Header/header.css";
import { connect } from "react-redux";

export class HeaderCart extends Component {
  render() {
    return (
      <div>
        <nav className="navbar navbar-default">
          <div className="container">
            <div className="navbar-header">
              <Link to="/" className="navbar-brand">
                <i className="fa fa-shopping-bag fa-lg" aria-hidden="true">
                  {" "}
                  <strong>NeoSTORE</strong>{" "}
                </i>
              </Link>
            </div>

            <div className="collapse navbar-collapse">
              {/* <ul className="nav navbar-nav ">
                <li>
                  <Link to="/order/cart">Bag</Link>
                </li>
                <li className="line" />
                <li>
                  <Link className="cursor" to="/order/del_address">
                    Delivery
                  </Link>
                </li>
                <li className="line" />
                <li>
                  <Link className="cursor" to="/order/cart">
                    Payment
                  </Link>
                </li>
              </ul> */}

              <ol className="nav navbar-nav navbar-right">
                <li>
                  <a href="#top">
                    <i className="fa fa-lock fa-lg" aria-hidden="true" /> 100%
                    SECURE
                  </a>
                </li>
              </ol>
            </div>
          </div>
        </nav>
        {this.props.children}
      </div>
    );
  }
}

export default HeaderCart;
