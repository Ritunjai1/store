import React, { Component } from "react";
import "./Login.css";
import { Formik, replace } from "formik";
import * as Yup from "yup";
import axios from "axios";
import { connect } from "react-redux";
import firebase from "firebase";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";
import { navigate } from "@reach/router";
import { ToastsContainer, ToastsStore } from "react-toasts";

interface IState {
  isSignedIn: boolean;
}
// Configure Firebase.
const config = {
  apiKey: "AIzaSyAk0eDT0IqJAb9M95bxCFisdJS4G0db478",
  authDomain: "social-login-5e7c1.firebaseapp.com"
  // ...
};
firebase.initializeApp(config);

// Configure FirebaseUI.
const uiConfig = {
  // Popup signin flow rather than redirect flow.
  signInFlow: "popup",
  // Redirect to /signedIn after sign in is successful. Alternatively you can provide a callbacks.signInSuccess function.
  signInSuccessUrl: "/signedIn",
  // We will display Google and Facebook as auth providers.
  signInOptions: [
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    firebase.auth.FacebookAuthProvider.PROVIDER_ID
  ]
};

class Login extends Component<any, any> {
  constructor(props: any) {
    super(props);
  }
  render() {
    return (
      <div className="container">
        <div className="container-login">
          <div className="col-md-5">
            <div className="panel panel-default">
              <div className="panel-heading">
                <h2 className="text-center text-muted">Login to NeoSTORE</h2>
              </div>
              <div className="panel-body">
                <p className="text-muted text-center">EASILY USING</p>
                <StyledFirebaseAuth
                  uiConfig={uiConfig}
                  firebaseAuth={firebase.auth()}
                />

                <p className="text-muted text-center">--OR USING--</p>
                <Formik
                  initialValues={{ email: "", password: "" }}
                  onSubmit={(values, { setSubmitting }) => {
                    console.log(values);
                    axios
                      .post("http://10.0.100.228:3000/login", values)
                      .then(response => {
                        console.log(response.data);
                        localStorage.setItem(
                          "address",
                          JSON.stringify(response.data.address)
                        );
                        localStorage.setItem(
                          "data",
                          JSON.stringify(response.data.data)
                        );
                        localStorage.setItem("token", response.data.token);

                        // REdux call
                        response.data.cart.map((item: any, index: any) => {
                          this.props.dispatch({
                            type: "CART_DATA",
                            payload: item.product_id
                          });
                        });
                        this.props.dispatch({
                          type: "USER_DATA",
                          payload: response.data
                        });
                        // alert(response.data.Message);
                        // debugger;
                        ToastsStore.success("Successfully logged in");
                        navigate("/", { replace: true });
                      })
                      .catch(error => {
                        // alert(error.message);
                        ToastsStore.error("Please check email and password");
                        console.log(error.response.message);
                      });
                  }}
                  validationSchema={Yup.object().shape({
                    email: Yup.string()
                      .email()
                      .required("Required"),
                    password: Yup.string()
                      .matches(
                        /^(?=.*\d).{4,8}$/,
                        "Must be one upper,lower and numeric char between 4 to 8 "
                      )
                      .required("Required")
                  })}
                >
                  {props => {
                    const {
                      values,
                      touched,
                      errors,
                      handleChange,
                      handleBlur,
                      handleSubmit
                    } = props;
                    return (
                      <form className="form-custom" onSubmit={handleSubmit}>
                        <div className="form-group">
                          <input
                            type="email"
                            className={
                              errors.email && touched.email
                                ? "form-control error"
                                : "form-control"
                            }
                            placeholder="Email Address"
                            name="email"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.email}
                          />
                          <small className="text-danger">
                            {errors.email && touched.email && (
                              <div className="input-feedback">
                                {errors.email}
                              </div>
                            )}
                          </small>

                          <input
                            type="password"
                            className={
                              errors.password && touched.password
                                ? "form-control error"
                                : "form-control"
                            }
                            placeholder="Password"
                            name="password"
                            onBlur={handleBlur}
                            onChange={handleChange}
                          />
                          <small className="text-danger">
                            {errors.password && touched.password && (
                              <div className="input-feedback">
                                {errors.password}
                              </div>
                            )}
                          </small>
                        </div>
                        <div className="form-group">
                          <button
                            className="btn btn-lg btn-primary btn-block"
                            type="submit"
                          >
                            Login
                          </button>
                        </div>
                      </form>
                    );
                  }}
                </Formik>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default connect(state => state)(Login);
