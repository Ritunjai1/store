import React, { Component } from "react";
import "./Orders.css";
import { Link } from "@reach/router";
import axios from "axios";
import moment from "moment";
import { navigate } from "@reach/router";

interface IState {
  order: Object[];
  orderArray: Object[];
  receipt: string;
}
class Orders extends Component<any, IState> {
  profile: string;
  profileObject: any;
  constructor(props: any) {
    super(props);
    this.profile = localStorage.getItem("data") || "";
    this.profileObject = JSON.parse(this.profile);
    this.state = {
      order: [],
      orderArray: [],
      receipt: ""
    };
  }
  componentDidMount() {
    axios
      .get("http://10.0.100.228:3000/order", {
        headers: {
          Authorization: localStorage.getItem("token")
        }
      })
      .then(response => {
        console.log(response.data.customer);
        let obj = response.data.customer;
        // // _.map(obj,(item)=>{return _.map(item.products,(product)=>{console.log(product);return product.order[0].product_image[0]})});
        // let orderArray = obj.map((element: any) => {
        //   return element.products.map((product: any) => {
        //     return product.orders[0].product_image[0];
        //   });
        // });

        const result = obj.map((item: any) => {
          return {
            id: item._id,
            orderPlacedDate: item.products[0].orderplaced,
            orderTotal: item.products[0].ordertotal,
            products: item.products.map((product: any) => {
              return {
                product_id: product.orders[0]._id,
                image: product.orders[0].product_image[0]
              };
            })
          };
        });

        this.setState({
          order: result
        });
      })
      .catch(error => {
        console.log(error);
      });
  }
  downLoad = (id: any) => {
    axios
      .post(
        "http://10.0.100.228:3000/invoice",
        { order: id },
        {
          headers: {
            Authorization: localStorage.getItem("token")
          }
        }
      )

      .then(response => {
        window.open(
          "http://10.0.100.228:3000/" + response.data.receipt,
          "_blank"
        );
      })
      .catch(error => {
        console.log(error);
      });
  };
  render() {
    return (
      <div className="container">
        <div className="container">
          <div className="container-fluid">
            <div className="col-md-12">
              <h4> My Account </h4>
              <hr />
            </div>

            <div className="col-md-3">
              <img
                src={
                  "http://10.0.100.228:3000/" + this.profileObject.profile_img
                }
                className="img-responsive img-circle profile"
                alt="profile_pic"
              />

              <div className="text-center">
                <br />
                <div className="text-danger">
                  <strong>{this.profileObject.first_name}</strong>
                </div>
              </div>
              <br />
              <ul className="nav">
                <li>
                  <Link to="#">
                    <i className="fa fa-pencil fa-fw" aria-hidden="true" />
                    &nbsp; Orders
                  </Link>
                </li>
                <li>
                  <Link to="/Profile">
                    <i className="fa fa-user fa-fw" aria-hidden="true" />
                    &nbsp; Profile
                  </Link>
                </li>
                <li>
                  <Link to="/Address">
                    <i
                      className="fa fa-address-book fa-fw"
                      aria-hidden="true"
                    />
                    &nbsp; Address
                  </Link>
                </li>
              </ul>
            </div>

            <div className="col-md-9">
              <div className="panel panel-default">
                {this.state.order.map((item: any) => (
                  <div>
                    <div className="panel-heading text-muted">
                      <span className="text-warning">
                        <strong>TRANSIT {item.id}</strong>
                      </span>

                      <div>
                        <span>{}</span>
                        <div>
                          <small>
                            Placed on:{" "}
                            {moment(item.orderPlacedDate).format("Do MMM YYYY")}
                          </small>
                          <small className="text-success">
                            <strong> ₹{item.orderTotal}</strong>
                          </small>
                        </div>
                        <hr />

                        <div className="panel-body">
                          <div className="row">
                            {item.products.map((element: any) => (
                              <div className="col-md-3">
                                <div className="thumbnail">
                                  <Link
                                    to={`/Pro_details/${element.product_id}`}
                                  >
                                    <img
                                      src={
                                        "http://10.0.100.228:3000/" +
                                        element.image
                                      }
                                      alt="product_image"
                                      className="img-rounded"
                                    />
                                  </Link>
                                </div>
                              </div>
                            ))}
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="panel-footer">
                      <button
                        type="button"
                        className="btn btn-success"
                        onClick={() => this.downLoad(item.id)}
                      >
                        Download invoice as PDF
                      </button>
                    </div>
                  </div>
                ))}
              </div>

              <hr />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Orders;
