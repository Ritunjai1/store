import React, { Component } from "react";
import "./Register.css";
import { Formik } from "formik";
import * as Yup from "yup";
import axios from "axios";
import { ToastsContainer, ToastsStore } from "react-toasts";

import firebase from "firebase";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";
import { navigate } from "@reach/router";
interface IState {
  isSignedIn: boolean;
}
// Configure Firebase.
const config = {
  apiKey: "AIzaSyAk0eDT0IqJAb9M95bxCFisdJS4G0db478",
  authDomain: "social-login-5e7c1.firebaseapp.com"
  // ...
};
// firebase.initializeApp(config);

// Configure FirebaseUI.
const uiConfig = {
  // Popup signin flow rather than redirect flow.
  signInFlow: "popup",
  // Redirect to /signedIn after sign in is successful. Alternatively you can provide a callbacks.signInSuccess function.
  signInSuccessUrl: "/signedIn",
  // We will display Google and Facebook as auth providers.
  signInOptions: [
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    firebase.auth.FacebookAuthProvider.PROVIDER_ID
  ]
};
class Register extends React.Component<{}, IState> {
  constructor(props: any) {
    super(props);
    this.state = {
      isSignedIn: false
    };
  }

  render() {
    return (
      <div className="container">
        <div className="container-register">
          <div className="col-md-5">
            <div className="panel panel-default">
              <div className="panel-heading">
                <h2 className="text-center text-muted">Register to NeoSTORE</h2>
              </div>
              <div className="panel-body">
                <p className="text-muted text-center">EASILY USING</p>
                <StyledFirebaseAuth
                  uiConfig={uiConfig}
                  firebaseAuth={firebase.auth()}
                />

                <p className="text-muted text-center">--OR USING--</p>

                <Formik
                  initialValues={{
                    first_name: "",
                    last_name: "",
                    email: "",
                    password: "",
                    cnfpassword: "",
                    phone: "",
                    gender: ""
                  }}
                  onSubmit={(values, { setSubmitting }) => {
                    console.log(values);
                    axios
                      .post("http://10.0.100.228:3000/registerUser", values)
                      .then(response => {
                        console.log(response.data);
                        alert(response.data.message);
                        ToastsStore.success("You are successfully Registered");
                        navigate("/");
                      })
                      .catch(error => {
                        console.log(error);
                        //alert("message");
                        ToastsStore.error(error.message);
                      });
                  }}
                  validationSchema={Yup.object().shape({
                    first_name: Yup.string().required("Required"),
                    last_name: Yup.string().required("Required"),
                    email: Yup.string()
                      .email()
                      .required("Required"),
                    password: Yup.string()
                      .matches(
                        /^(?=.*\d).{4,8}$/,
                        " Password must be between 4 and 8 digits long and include at least one numeric digit"
                      )
                      .min(
                        8,
                        " Password must be between 4 and 8 digits long and include at least one numeric digit."
                      )
                      .required("Required"),
                    cnfpassword: Yup.string()
                      .oneOf([Yup.ref("password")], "Must be same as password")
                      .required("Required"),
                    phone: Yup.string()
                      .matches(/^[6-9]\d{9}$/, "Must be 10 digits")
                      .required("Required"),
                    gender: Yup.string().required("Required")
                  })}
                >
                  {props => {
                    const {
                      values,
                      touched,
                      errors,
                      handleChange,
                      handleBlur,
                      handleSubmit
                    } = props;
                    return (
                      <form className="form-custom" onSubmit={handleSubmit}>
                        <div className="form-group">
                          <input
                            type="text"
                            className={
                              errors.first_name && touched.first_name
                                ? "form-control error"
                                : "form-control"
                            }
                            placeholder="First Name"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            name="first_name"
                            value={values.first_name}
                          />
                          <small className="text-danger">
                            {errors.first_name && touched.first_name && (
                              <div className="input-feedback">
                                {errors.first_name}
                              </div>
                            )}
                          </small>
                          <input
                            type="text"
                            className={
                              errors.last_name && touched.last_name
                                ? "form-control error"
                                : "form-control"
                            }
                            placeholder="Last Name"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            name="last_name"
                            value={values.last_name}
                          />
                          <small className="text-danger">
                            {errors.last_name && touched.last_name && (
                              <div className="input-feedback">
                                {errors.last_name}
                              </div>
                            )}
                          </small>
                          <input
                            type="email"
                            className={
                              errors.email && touched.email
                                ? "form-control error"
                                : "form-control"
                            }
                            placeholder="Your Email Address"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            name="email"
                            value={values.email}
                          />
                          <small className="text-danger">
                            {errors.email && touched.email && (
                              <div className="input-feedback">
                                {errors.email}
                              </div>
                            )}
                          </small>

                          <input
                            type="password"
                            className={
                              errors.password && touched.password
                                ? "form-control error"
                                : "form-control"
                            }
                            placeholder="Choose Password"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.password}
                            name="password"
                          />
                          <small className="text-danger">
                            {errors.password && touched.password && (
                              <div className="input-feedback">
                                {errors.password}
                              </div>
                            )}
                          </small>

                          <input
                            type="password"
                            className={
                              errors.cnfpassword && touched.cnfpassword
                                ? "form-control error"
                                : "form-control"
                            }
                            placeholder="Confirm Password"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.cnfpassword}
                            name="cnfpassword"
                          />
                          <small className="text-danger">
                            {errors.cnfpassword && touched.cnfpassword && (
                              <div className="input-feedback">
                                {errors.cnfpassword}
                              </div>
                            )}
                          </small>

                          <input
                            type="number"
                            className={
                              errors.phone && touched.phone
                                ? "form-control error"
                                : "form-control"
                            }
                            placeholder="Enter Phone Number"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            name="phone"
                            value={values.phone}
                          />
                          <small className="text-danger">
                            {errors.phone && touched.phone && (
                              <div className="input-feedback">
                                {errors.phone}
                              </div>
                            )}
                          </small>

                          <legend className="gender-legend">I'm</legend>
                          <div className="checkbox">
                            <label>
                              <input
                                // className={
                                //   errors.gender && touched.gender
                                //     ? "form-control error"
                                //     : "form-control"
                                // }
                                type="radio"
                                value={0}
                                name="gender"
                                onChange={handleChange}
                                onBlur={handleBlur}
                              />
                              <strong>Male</strong>{" "}
                            </label>
                            <label>
                              <input
                                // className={
                                //   errors.gender && touched.gender
                                //     ? "form-control error"
                                //     : "form-control"
                                // }
                                type="radio"
                                value={1}
                                name="gender"
                                onChange={handleChange}
                                onBlur={handleBlur}
                              />
                              <strong>Female</strong>{" "}
                            </label>
                          </div>
                          <small className="text-danger">
                            {errors.gender && touched.gender && (
                              <div className="input-feedback">
                                {errors.gender}
                              </div>
                            )}
                          </small>

                          <button
                            type="submit"
                            className="btn btn-lg btn-primary btn-block"
                          >
                            Register
                          </button>
                        </div>
                      </form>
                    );
                  }}
                </Formik>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Register;
