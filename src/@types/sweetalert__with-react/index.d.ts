declare module "@sweetalert/with-react" {
  import React from "react";

  export function getDOMNodeFromJSX(
    Element: React.Component<any, any>
  ): Promise;

  export function swal(...args: any[]): void;
}
