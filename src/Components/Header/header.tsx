import React, { Component } from "react";
import "./header.css";
import { Link } from "@reach/router";
import { connect } from "react-redux";
import axios from "axios";
import { ToastsStore } from "react-toasts";
import throttle from "lodash.throttle";
// import { debounce } from "lodash";
interface IProps {
  autocomplete: true;
  UserDataReducer: any;
  logout: any;
  input: string;
}

class Header extends Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      input: ""
    };
  }
  search = (e: any) => {
    console.log(this.state.input);
    this.setState({
      input: e.target.value
    });

    axios
      .get("http://10.0.100.228:3000/findproducts?text=" + this.state.input)
      .then(response => {
        console.log(response.data.product);
      })
      .catch(error => {
        console.log(error);
      });
  };
  logout(e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) {
    localStorage.removeItem("data");
  }

  public render() {
    let login = this.props.UserDataReducer.isLoggedIn;
    return (
      <div>
        <nav className="navbar navbar-default">
          <div className="container">
            <div className="row">
              <div className="col-md-4">
                <div className="navbar-header">
                  <button
                    type="button"
                    className="navbar-toggle collapsed"
                    data-toggle="collapse"
                    data-target="#navbar"
                    aria-expanded="false"
                  >
                    <span className="sr-only">Toggle navigation</span>
                    <span className="icon-bar" />
                    <span className="icon-bar" />
                    <span className="icon-bar" />
                  </button>
                  <Link to="/" className="navbar-brand">
                    <i className="fa fa-shopping-bag fa-lg" aria-hidden="true">
                      {" "}
                      <strong>NeoSTORE</strong>
                    </i>
                  </Link>
                </div>
              </div>
              <div className="collapse navbar-collapse" id="navbar">
                <form className="pull-left">
                  <div className="row">
                    <div className="col-md-8" id="bar">
                      <div className="form-group">
                        <div className="input-group search-bar">
                          <input
                            // name="search"
                            className="form-control"
                            name={this.state.input}
                            onChange={this.search}
                          />
                          <span className="input-group-btn">
                            <button
                              type="submit"
                              className="btn btn-default"
                              onClick={this.search}
                            >
                              <i className="fa fa-search" />
                            </button>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>

                <ul className="nav navbar-nav navbar-right">
                  <li className="dropdown">
                    <a
                      href="#"
                      className="dropdown-toggle"
                      data-toggle="dropdown open"
                      role="button"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      <i className="fa fa-user fa-lg" area-hidden="true" />
                      <span className="caret" />
                    </a>

                    <ul className="dropdown-content">
                      {login ? (
                        <div>
                          <li>
                            <Link className="drop" to="Profile">
                              <i
                                className="fa fa-user fa-fw"
                                aria-hidden="true"
                              />
                              &nbsp; Profile
                            </Link>
                          </li>
                          <li>
                            <Link className="drop" to="Orders">
                              <i
                                className="fa fa-list-alt fa-fw"
                                aria-hidden="true"
                              />
                              &nbsp; Orders
                            </Link>
                          </li>
                          <li>
                            <Link className="drop" to="Address">
                              <i
                                className="fa fa-address-card-o fa-fw"
                                aria-hidden="true"
                              />
                              &nbsp; Addresses
                            </Link>
                          </li>
                          <li>
                            <a
                              href="/"
                              className="drop"
                              onClick={() => {
                                axios
                                  .post(
                                    "http://10.0.100.228:3000/cart",
                                    this.props.CartDataReducer,
                                    {
                                      headers: {
                                        Authorization: localStorage.getItem(
                                          "token"
                                        )
                                      }
                                    }
                                  )
                                  .then(response => {
                                    ToastsStore.success(
                                      "Logged out successfully"
                                    );
                                    console.log(response);
                                  })
                                  .catch(error => {
                                    alert(error);
                                    console.log(error);
                                  });
                                let keysToRemove = [
                                  "data",
                                  "token",
                                  "address",
                                  "Neostore"
                                ];

                                for (let i = 0; i < keysToRemove.length; i++) {
                                  localStorage.removeItem(keysToRemove[i]);
                                }
                              }}
                            >
                              <i
                                className="fa fa-sign-out fa-fw"
                                aria-hidden="true"
                              />
                              &nbsp; Logout
                            </a>
                          </li>
                        </div>
                      ) : (
                        <div>
                          <li>
                            <Link className="drop" to="Login">
                              <i
                                className="fa fa-sign-in fa-fw"
                                aria-hidden="true"
                              />
                              &nbsp; Login
                            </Link>
                          </li>

                          <li>
                            <Link className="drop" to="Register">
                              <i
                                className="fa fa-user-plus fa-fw"
                                aria-hidden="true"
                              />
                              &nbsp; Register
                            </Link>
                          </li>
                        </div>
                      )}
                    </ul>
                  </li>
                </ul>

                <ul className="nav navbar-nav navbar-right">
                  <li>
                    <Link className="cart" to="order/cart">
                      <i
                        className="fa fa-shopping-cart fa-lg"
                        aria-hidden="true"
                      />
                      Cart{" "}
                      <span className="badge">
                        {this.props.CartDataReducer.length}
                      </span>
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </nav>
        {this.props.children}
      </div>
    );
  }
}
export default connect(state => state)(Header);
// debounceEvent(...args){
//     this.debouncedEvent = debounce(...args);
//     return e =>
//     {
//         e.persist();
//         return this.debouncedEvent(e);
//     }

// }
