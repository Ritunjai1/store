import React, { Component } from "react";
import { Link } from "@reach/router";
import styled from "styled-components";
const Div = styled.div`
  text-align: -webkit-center;
`;

export default class ErrorNotFound extends Component {
  render() {
    return (
      <Div id="error">
        <h1 className="notFoundTitle">Oops! That page can’t be found.</h1>
        <p className="notFoundDesc">
          It looks like nothing was found at this location. Maybe try one of the
          links in the menu or press home to go to main page
          <Link to="/">Home</Link>
        </p>
      </Div>
    );
  }
}
