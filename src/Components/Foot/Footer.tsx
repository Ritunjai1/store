import React, { Component } from "react";
import "./Footer.css";
import { Link } from "@reach/router";

class Footer extends Component {
  constructor(props: any) {
    super(props);
  }
  render() {
    return (
      <div>
        <footer className="footer">
          <div className="container text-center">
            <div className="row">
              <div className="col-md-4">
                <h3>About Company</h3>
                <div>
                  <p>
                    NeoSOFT Technologies is here at your quick and easy service
                    for shopping .
                  </p>
                  <h6>Contact information</h6>
                  <p>Email: contact@neosofttech.com</p>
                  <p>Phone: +91 0000000000</p>
                  <p>MUMBAI, INDIA</p>
                </div>
              </div>

              <div className="col-md-4">
                <h3>Information</h3>
                <ul className="list-unstyled">
                  <li>
                    <Link to="terms" className="list">
                      Terms &amp; Conditions
                    </Link>
                  </li>
                  <li>
                    <Link to="guarantee" className="list">
                      Guarantee &amp; Return Policy
                    </Link>
                  </li>
                  <li>
                    <Link to="contact" className="list">
                      Contact us
                    </Link>
                  </li>
                  <li>
                    <Link to="privacy" className="list">
                      Privacy Policy
                    </Link>
                  </li>
                  <li>
                    <Link to="locate_us" className="list">
                      Locate Us
                    </Link>
                  </li>
                </ul>
              </div>

              <div className="col-md-4">
                <h3>Newsletter</h3>
                <form>
                  <div>
                    <p>
                      Sign up to get exclusive offers from our favorite brands
                      and to be well up in the news.
                    </p>
                    <div className="form-group">
                      <input
                        type="text"
                        className="form-control"
                        placeholder="your email..."
                      />
                    </div>
                    <button type="button" className="btn btn-default">
                      Subscribe
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>

          <div className="container">
            <p className="copywrite text-center">
              Copyright © 2017 NeoSOFT Technologies All rights reserved | Design
              by{" "}
              <a id="link" href="http://www.neosofttech.com/">
                NeoSOFT Technologies
              </a>
            </p>
          </div>
        </footer>
      </div>
    );
  }
}
export default Footer;
