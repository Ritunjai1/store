import React from "react";
import { Router, Redirect } from "@reach/router";

export default function Route({ children }: any) {
  const token = localStorage.getItem("token");

  if (token) {
    return children;
  }
  return <Redirect to="/Login" noThrow />;
}
